# pwa-install weboot

A [weboot](https://weboot.tech) to display an "install" toast for Progressive Web Apps (PWAs). This weboot wraps the pwa-install web component.

## Usage

This component is a single file, but requires bootstrap js and css, as well as the pwa-install web component to be loaded first. It is recommended to load pwa-install as a module.

Once the script and pre-requisites are loaded, you simply need to include `<pwa-install id="pwa-install"></pwa-install>` in your document body.

```
<head>
  <link rel="stylesheet" src="/src/css/bootstrap/bootstrap.css"></link>
  <script src="/src/js/bootstrap/bootstrap.js"></script>
  <script type="module" src="/src/js/components/webcomponents/pwa-install/pwa-install.js"></script>
  <script type="module" src="/src/js/components/weboot/pwa-install/pwa-install.js"></script>
</head>
<body>
  <pwa-install id="pwa-install"></pwa-install>
</body>
```

## Localization

The text of the banner can be localized by setting a "lang" in your app's manifest file. Right now the following languages are supported:

 + [x] en (default)
 + [x] es

