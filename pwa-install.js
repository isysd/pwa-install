const pwaInstall = document.getElementById('pwa-install');

const style = `
    #pwa-install-toast {
      position: fixed;
      top: 10px;
      right: 10px;
      min-width: 300px;
      z-index: 99999;
    }

    @media only screen (max-width: 800px){
      #pwa-install-toast {
        min-width: 240px;
      }
    }
`

const LANG = {
 'en': {
   "Install": "Install",
   "or browse.": "or browse.",
 },
 'es': {
   "Install": "Instalar",
   "or browse.": "o usar.",
 }
}

async function getManifest() {
  let manifestUrl = '/manifest.json'
  let manifestElement = document.querySelector('link[rel="manifest"]')
  if (manifestElement) {
    manifestUrl = manifestElement.getAttribute('href');
  }
  return (await fetch(manifestUrl)).json()
}

if (pwaInstall) {
  pwaInstall.addEventListener('pwa-install-available', async (e) => {
    if (pwaInstall.getInstalledRelatedApps && typeof(pwaInstall.getInstalledRelatedApps()) === 'undefined') {
      const manifest = await getManifest()
      var language = manifest.lang || window.navigator.userLanguage || window.navigator.language || 'en';
      language = language.slice(0, 2)
      if (!LANG.hasOwnProperty(language)) {
        console.info(`Sorry, ${language} is not supported. Defaulting to english for PWA install banner.`)
        language = 'en' // Default to en if not supported
      }
      if (manifest && manifest.icons) {
        pwaInstall.innerHTML = `
  <style>
${style}
  </style>
    <div class="toast" id="pwa-install-toast" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="toast-header">
        <img src="${manifest.icons[manifest.icons.length - 1].src}" class="rounded mr-2" alt=${manifest.name}" style="height: 1em;">
        <strong class="mr-auto">${LANG[language]['Install']} ${manifest.name}</strong>
        <button type="button" class="ml-2 mb-1 close text-danger" data-dismiss="toast" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="toast-body">
        <a onclick="javascript:document.getElementById('pwa-install').install()">${LANG[language]['Install']} ${manifest.name}</a> ${LANG[language]['or browse.']}
      </div>
    </div>
`
      }
      try {
        $('#pwa-install-toast').toast({autohide: true, delay: 15000})
        $('#pwa-install-toast').toast('show')
      } catch(e) {
        console.warn(e)
      }
      $('#pwa-install-toast').on('hidden.bs.toast', function () {
	var el = document.getElementById("pwa-install-toast");
        el.parentNode.removeChild(el);})
      }
  });
  pwaInstall.addEventListener('pwa-install-installed', (e) => {
    try {
      $('#pwa-install-toast').toast('dispose')
    } catch(e) {
      console.warn(e)
    }
  });
  pwaInstall.addEventListener('pwa-install-error', (e) => {
    try {
      $('#pwa-install-toast').toast('dispose')
    } catch(e) {
      console.warn(e)
    }
  });
}

